using System;
interface IPasswordValidatorService
{
	bool IsValidPassword(String candidate);
}
