using System;
class TestEmailValidator {
	public static void Main() {
		IEmailValidatorService emailValidator=new EmailValidator();
		IPasswordValidatorService passwordValidator=new PasswordValidator(8);
		string[] emails={"teo.samarzija@gmail.com","teo.samarzija@teos-acer-laptop","gmail.com"};
		string[] passwords={"password","P4ssw0rd","password123"};
		foreach (string email in emails) {
			bool isValid = emailValidator.IsValidAddress(email);
			Console.WriteLine('"' + email + "\" " + ((isValid)?("is"):("is not")) + " a valid e-mail address.");
		}
		foreach (string password in passwords) {
			bool isValid=passwordValidator.IsValidPassword(password);
			Console.WriteLine('"' + password + "\" " + ((isValid)?("is"):("is not")) + " a valid password.");
		}
	}
}
