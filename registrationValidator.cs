class RegistrationValidator : IRegistrationValidator {
	public IEmailValidatorService emailValidator {get; set;}
	public IPasswordValidatorService passwordValidator{get; set;}
	public RegistrationValidator() {
		emailValidator=new EmailValidator();
		passwordValidator=new PasswordValidator(8);
	}
	public bool IsUserEntryValid(UserEntry entry) {
		return emailValidator.IsValidAddress(entry.Email)
			&& passwordValidator.IsValidPassword(entry.Password);
	}
}
