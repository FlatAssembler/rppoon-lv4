using System;
class TestUserEntryValidator {
	public static void Main() {
		IRegistrationValidator registrationValidator=new RegistrationValidator();
		while (true) {
			UserEntry userEntry=UserEntry.ReadUserFromConsole();
			if (registrationValidator.IsUserEntryValid(userEntry))
				break;
			else
				Console.WriteLine("Invalid input!");
		}
	}
}
