using System.Collections.Generic;
using System;
class TestBook {
	public static void Main() {
		RentingConsolePrinter printer=new RentingConsolePrinter();
		List<IRentable> itemsToBePrinted=new List<IRentable>();
		itemsToBePrinted.Add(new Book("Don Quixote"));
		itemsToBePrinted.Add(new Video("Ko to tamo peva"));
		Console.WriteLine("The total price is:");
		printer.PrintTotalPrice(itemsToBePrinted);
		Console.WriteLine("The items in the list are:");
		printer.DisplayItems(itemsToBePrinted);	
	}
}
