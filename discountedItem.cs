using System;
class DiscountedItem : RentableDecorator
{
	public double discount = 0.2;
	public DiscountedItem(IRentable rentable) : base(rentable) { }
	public override double CalculatePrice()
	{
		return base.CalculatePrice() * (1 - discount);
	}
	public override String Description
	{
		get
		{
			return base.Description + " - now at " + (discount * 100)+ "% off!";
		}
	}
}
