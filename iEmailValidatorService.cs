using System;
interface IEmailValidatorService
{
	bool IsValidAddress(String candidate);
}
