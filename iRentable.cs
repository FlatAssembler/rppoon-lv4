using System;
interface IRentable
{
	String Description { get; }
	double CalculatePrice();
}
