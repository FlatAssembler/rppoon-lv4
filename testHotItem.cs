using System.Collections.Generic;
using System;
class TestHotItem {
	public static void Main() {
		RentingConsolePrinter printer=new RentingConsolePrinter();
		List<IRentable> itemsToBePrinted=new List<IRentable>();
		itemsToBePrinted.Add(new Book("Don Quixote"));
		itemsToBePrinted.Add(new Video("Ko to tamo peva"));
		itemsToBePrinted.Add(new HotItem(new Book("Heidi - life in mountains")));
		itemsToBePrinted.Add(new HotItem(new Video("Surogat")));
		Console.WriteLine("The total price is:");
		printer.PrintTotalPrice(itemsToBePrinted);
		Console.WriteLine("The items in the list are:");
		printer.DisplayItems(itemsToBePrinted);	
	}
}
