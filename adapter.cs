using System.Collections.Generic;
using System;
class Adapter : IAnalytics
{
	private Analyzer3rdParty analyticsService;
	public Adapter(Analyzer3rdParty service)
	{
		this.analyticsService = service;
	}
	private double[][] ConvertData(Dataset dataset)
	{
		IList<List<double>> sourceData=dataset.GetData();
		int numberOfRows=sourceData.Count;
		int numberOfColumns=sourceData[0].Count;
		double[][] data=new double[numberOfRows][];
		for (int i=0; i<numberOfRows; i++) //Allocating the memory for the array.
			data[i]=new double[numberOfRows];
		for (int i=0; i<numberOfRows; i++) //Copying the 2D list into 2D array.
			for (int j=0; j<numberOfRows; j++)
				data[i][j]=sourceData[i][j];
		return data;
	}
	public double[] CalculateAveragePerColumn(Dataset dataset)
	{
		double[][] data = this.ConvertData(dataset);
		return this.analyticsService.PerColumnAverage(data);
	}
	public double[] CalculateAveragePerRow(Dataset dataset)
	{
		double[][] data = this.ConvertData(dataset);
		return this.analyticsService.PerRowAverage(data);
	}
}
