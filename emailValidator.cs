using System;
class EmailValidator : IEmailValidatorService {
	public bool IsValidAddress(string candidate) {
		if (candidate.Length < 4)
			return false;
		if (candidate.Substring(candidate.Length - (".com".Length)) != ".com"
			&& candidate.Substring(candidate.Length - (".hr".Length)) != ".hr")
			return false;
		if (candidate.IndexOf("@")==-1)
			return false;
		return true;
	}
}
