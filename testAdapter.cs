using System;
using System.Linq;
class TestAdapter {
	public static void Main(){
		Dataset datasetToReadFile=new Dataset("example.csv");
		IAnalytics analyzer=new Adapter(new Analyzer3rdParty());
		double[] averagePerColumn=analyzer.CalculateAveragePerColumn(datasetToReadFile);
		double[] averagePerRow=analyzer.CalculateAveragePerRow(datasetToReadFile);
		Console.WriteLine("The average per rows is:");
		foreach (double number in averagePerRow)
			Console.Write(number+" ");
		Console.WriteLine("");
		Console.WriteLine("The total average per rows is: "+averagePerRow.Average());
		Console.WriteLine("The average per columns is:");
		foreach (double number in averagePerColumn)
			Console.Write(number+" ");
		Console.WriteLine("");
		Console.WriteLine("The total average per columns is: "+averagePerColumn.Average());
	}
}
